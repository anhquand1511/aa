/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author admin
 */
//@WebServlet(name= "TinhV2", urlPatterns = {"/tinhv2"})
public class TinhV2 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException {
        String dai = request.getParameter("dai");
        String rong = request.getParameter("rong");
        PrintWriter out = respone.getWriter();
//        String cv = request.getParameter("cv");
//        String dt = request.getParameter("dt");
        String daa[] = request.getParameterValues("d");
        double d, r;
        String ms = "";
        try {
            d = Double.parseDouble(dai);
            r = Double.parseDouble(rong);
//            cach 1
//            if ((cv == null) && (dt == null)) {
//                ms = "Khong chon gi";
//            } else if (cv != null && dt == null) {
//                ms = "chu vi: " + ((d + r) * 2);
//            } else if (cv == null && dt != null) {
//                ms = "dien tich: " + (d * r);
//            } else {
//                ms = "<p>chu vi: " + ((d + r) * 2) + "<br/>dien tich: " + (d * r) + "</p>";
//            }
//               cach 2
            if (daa == null) {
                ms = "Khong chon gi";
            } else if (daa.length == 2) {
                ms = "<p>chu vi: " + ((d + r) * 2) + "<br/>dien tich: " + (d * r) + "</p>";
            } else if (daa[0].equals("1")) {
                ms = "dien tich: " + (d * r);
            } else {
                ms = "chu vi: " + ((d + r) * 2);
            }
            out.println(ms);
        } catch (NumberFormatException e) {
            System.out.println(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse respone) throws ServletException, IOException {
        request.getRequestDispatcher("shapev2.html").forward(request, respone);
    }

}
