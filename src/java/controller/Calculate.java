/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
public class Calculate extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String cal(double n1, double n2, String op) {
        String rs = "";
        switch (op) {
            case "+":
                rs = n1 + " + " + n2 + " = " + (n1 + n2);
                break;
            case "-":
                rs = n1 + " - " + n2 + " = " + (n1 - n2);
                break;
            case "*":
                rs = n1 + " * " + n2 + " = " + (n1 * n2);
                break;
            case "/":
                if (n2 == 0) {
                    System.out.println("cannot devided by 0");
                    break;
                } else {
                    rs = n1 + " / " + n2 + " = " + (n1 / n2);
                }
                break;
        }
        return rs;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Calculate</title>");
            out.println("</head>");
            out.println("<body>");
//            out.println("<h1>Servlet Calculate at " + request.getContextPath () + "</h1>");
            String n1 = request.getParameter("num1");
            String n2 = request.getParameter("num2");
            String option = request.getParameter("op");
            double num1, num2;
            String rs;
            try {
                num1 = Double.parseDouble(request.getParameter("num1"));
                num2 = Double.parseDouble(request.getParameter("num2"));
                rs = cal(num1, num2, option);
                out.println("<h1 style='color:chocolate'>" + rs + "</h1>");
            } catch (NumberFormatException e) {
                response.sendRedirect("math.html");
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
